package client;

import game.Board;
import game.Piece;
import players.HumanPlayer;
import players.Player;

import java.util.Observable;

public class Game extends Observable {
    public static final String BOARD_UPDATE = "BOARD_UPDATE";
    public static final String PLAYER_UPDATE = "PLAYER_UPDATE";

	private Board board;
	private Player p;
    private Client client;
    private String p1;
    private String p2;

    private String currentPlayer = null;
	
	public Game(Client client, String p1, String p2) {
        this.client = client;
        this.p1 = p1;
        this.p2 = p2;

        board = new Board();
    }

	public void start() {
        setChanged();
        notifyObservers(BOARD_UPDATE);
    }

    public void doMove(String player, int x, int y) {
        if (player.equals(p1)) {
            board.addPiece(x, y, Piece.YELLOW);
        } else {
            board.addPiece(x, y, Piece.RED);
        }

        setChanged();
        notifyObservers(BOARD_UPDATE);
    }

	public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public String getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(String currentPlayer) {
        this.currentPlayer = currentPlayer;

        setChanged();
        notifyObservers(PLAYER_UPDATE);
    }
    
    public Piece getPiece(String player) {
        if (player.equals(p1)) {
            return Piece.YELLOW;
        } else {
            return Piece.RED;
        }
    }
}
