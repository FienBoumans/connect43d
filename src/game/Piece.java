package game;

public enum Piece {
	RED, YELLOW;

    public Piece other() {
        if (this == RED) {
            return YELLOW;
        } else {
            return RED;
        }
    }
}

