package client;

public enum State {
    DISCONNECTED, CONNECTED, READY, PLAY;
}
