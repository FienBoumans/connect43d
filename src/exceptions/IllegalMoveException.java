package exceptions;

public class IllegalMoveException extends ProtocolException {
	public IllegalMoveException() {
		super("Illegal move");
	}
}
