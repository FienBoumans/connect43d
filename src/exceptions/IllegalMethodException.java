package exceptions;

public class IllegalMethodException extends ProtocolException {
	public IllegalMethodException(){
		super("Illegal use of method");
	}

}
