package client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

import game.*;
import players.ComputerPlayer;
import players.HumanPlayer;
import players.Player;
import players.RandomPlayer;
import protocol.Protocol;

public class Client implements Protocol {
	private static final String EXTENSIONS = " ";

	private String playerType = null;
	private String name;
	private Game game;
	private Player player;

	private ServerConnection serverConnection = null;
	private State gameState = State.DISCONNECTED;
	private ClientTUI clientTUI;

	public Client() {
		clientTUI = new ClientTUI(this);
	}

	public void start() {
		clientTUI.start();
	}
	
	public void setPlayer(String playerType) {
		if (playerType.equals("H")) {
			player = new HumanPlayer(name, Piece.RED);
		} else if (playerType.equals("E")) {
			player = new RandomPlayer(name, Piece.RED);
		} else if (playerType.equals("D")) {
			player = new ComputerPlayer(name,Piece.RED);
		} else {
			clientTUI.printMessage("PlayerType is not recognized");
		}
	}

	public void connect(String name, InetAddress address, int port) {
		synchronized (gameState) {
			try {
				Socket socket = new Socket(address, port);
				serverConnection = new ServerConnection(this, socket);
				serverConnection.sendConnect(name, Client.EXTENSIONS);
				serverConnection.receiveConfirm();

				gameState = State.CONNECTED;
				clientTUI.printMessage("connected to server");

				new Thread(serverConnection).start();
			} catch (IOException e) {
				clientTUI.printMessage("could not connect to server");
			}
		}
	}

	public void disconnect() {
		synchronized (gameState) {
			gameState = State.DISCONNECTED;

			serverConnection.sendDisconnect();
			serverConnection.shutDown();

			clientTUI.printMessage("disconnected");
		}
	}

	public void serverDisconnect() {
		synchronized (gameState) {
			gameState = State.DISCONNECTED;
			clientTUI.printMessage("server disconnected");
		}
	}

	public void ready() {
		synchronized (gameState) {
			this.gameState = State.READY;

			serverConnection.sendReady();
		}
	}

	public void unready() {
		synchronized (gameState) {
			this.gameState = State.READY;

			serverConnection.sendUnready();
		}
	}

	public void startGame(String[] playerNames) {
		synchronized (gameState) {
			this.gameState = State.PLAY;
			this.game = new Game(this, playerNames[0], playerNames[1]);

			clientTUI.printMessage("starting game");

			this.game.addObserver(clientTUI);
			this.game.start();
			this.game.setCurrentPlayer(playerNames[0]);
		}
	}

	public void humanMove(int move) {
		int x = move % Board.DIM;
		int y = move / Board.DIM;

		if (!name.equals(game.getCurrentPlayer())) {
			clientTUI.printMessage("it is not your turn");
		} else if (game.getBoard().isValidMove(x, y)) {
			serverConnection.sendMove(x, y);
		} else {
			clientTUI.printMessage("invalid move");
		}
	}
	
	public void otherMove() {
		int move = player.determineMove(game.getBoard());
		int x = move % Board.DIM;
		int y = move / Board.DIM;

		if (!name.equals(game.getCurrentPlayer())) {
			clientTUI.printMessage("it is not your turn");
		} else if (game.getBoard().isValidMove(x, y)) {
			serverConnection.sendMove(x, y);
		} else {
			clientTUI.printMessage("invalid move");
		}
	}


	public void serverMove(String player, int x, int y, String nextPlayer) {
		if (gameState == State.PLAY) {
			game.doMove(player, x, y);
			game.setCurrentPlayer(nextPlayer);
		}
	}

	public void serverMove(String player, int x, int y) {
		if (gameState == State.PLAY) {
			game.doMove(player, x, y);
			game.setCurrentPlayer("");
		}
	}

	public void endDraw() {
		synchronized (gameState) {
			if (gameState == State.PLAY) {

				gameState = State.CONNECTED;
				game.deleteObserver(clientTUI);
				game = null;

				clientTUI.printMessage("the game ended in a draw");
			}
		}
	}
	
	public int hint(){
		return new ComputerPlayer(getName(), game.getPiece(name)).determineMove(game.getBoard());
	}

	public void endWinner(String name) {
		synchronized (gameState) {
			if (gameState == State.PLAY) {
				gameState = State.CONNECTED;
				game.deleteObserver(clientTUI);
				game = null;

				if (name.equals(this.name)) {
					clientTUI.printMessage("you won the game!");
				} else {
					clientTUI.printMessage(name + " won the game");
				}
			}
		}
	}

	public void error(String message) {
		clientTUI.printMessage(String.format(Protocol.ERROR, Protocol.Error.ILLEGAL_METHOD_USE));
	}

	// getters and setters
	public boolean isMyTurn() {
		return true; // return game.getCurrentPlayerName().equals(name);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public State getGameState() {
		return gameState;
	}

	public void setGameState(State gameState) {
		this.gameState = gameState;
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}
	
	public Player getPlayer() {
		return player;
	}

	public static void main(String[] args) {
		Client client = new Client();
		client.start();
	}
}
