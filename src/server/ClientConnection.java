package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import protocol.Protocol;

public class ClientConnection extends Thread {
	
	private BufferedReader in;
	private BufferedWriter out;
	private ClientController ch;
	private Status status;
	boolean running = false;
	private String name;

	/**
	 * Constructs a ClientConnection object
	 * @param ch ClientController where the clientConnection belongs to
	 * @param sock Socket that will be used by the ClientConnection
	 */
	//@ requires ch != null && sock != null;
	public ClientConnection(ClientController ch, Socket sock) {
		try {
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
			this.ch = ch;
		} catch (IOException e) {
			stopClient();
		}
	}

	/**
	*Reads the messages in the socket connection. Each message will
	*be forwarded to the method receiveFromClient in this class.
	*/
	public void run() {
		String line;
		running = true;
		try {
			line = in.readLine();
			System.out.println("Waiting on client that has to receive command");
			while (running && line != null) {
				System.out.println("Command received from client: " + line);
				receiveFromClient(line);
				line = in.readLine();
			}
		} catch (IOException e) {
			stopClient();
		}

	}

	/**
	 * This method takes care of determining what there has to be done with 
	 * the incoming messages and call this method.
	 * @param line String that will received from the socket
	 * @throws IOException 
	 */
	/*
	 *@ requires line != null;
	 *@ requires line.startsWith(Protoco.CONNECT_STRING) && this.status == null
	 *@ ensures this.status.equals(Status.CONNECT);
	 *also
	 *@ requires line.equals(Protocol.READY) && status.equals(Status.CONNECT);
	 *@ ensures this.status.equals(Status.READY);  
	 *also
	 *@ requires line.equals(Protocol.UNREADY) && status.equals(Status.READY);
	 *@ ensures this.status.equals(Status.CONNECT); 
	 */
	public void receiveFromClient(String line) throws IOException {
		if (line.startsWith(Protocol.CONNECT_STRING) && this.status == null) {
			sentConfirm(line);
			this.status = Status.CONNECT;
		} else if (line.equals(Protocol.DISCONNECT)) {
			if (this.status.equals(Status.PLAYING)) {
				stopClient();
			} else if (this.status != null) {
				stopClient();
			}
		} else if (line.equals(Protocol.READY) && status.equals(Status.CONNECT)) {
			System.out.println("command is ready");
			this.status = Status.READY;
			ch.checkforOpponent(this);
		} else if (line.startsWith(Protocol.UNREADY) && status.equals(Status.READY)) {
			this.status = Status.CONNECT;
			System.out.println("User write unready, new status is connect");
		} else if (line.startsWith(Protocol.CLIENT_MOVE_STRING) && status.equals(Status.PLAYING)) {
			System.out.println("Command client move received");
			doMove(line);
		} else {
			out.write(String.valueOf(Protocol.Error.ILLEGAL_METHOD_USE));
		}
	}

	/**
	 * Write confirm to the socket if the incoming line is correct. It checks
	 * if there is received a name and if this name is not already in use. 
	 * Otherwise it writes an error to the socket.
	 * @param line String that is received from the socket.
	 */
	/*
	 * @ requires line != null;
	 * @ requires line.split(" ").length >= 2;
	 * @ ensures name.equals(line.split(" ")[1]);
	 * 
	 */
	public void sentConfirm(String line) {
		System.out.println("Command is connect");
		System.out.println(line);
		String[] words = line.split(" ");
		if (words[1] != null) {
			String tempname = words[1];
			System.out.println("checking all connections");
			for (ClientConnection cc : ch.getConnections()) {
				
				if (cc != this && cc.getClientName().equals(tempname)) {
					try {
						out.write(String.format(Protocol.ERROR, Protocol.Error.USER_ALREADY_CONNECTED.code));
						out.newLine();
						out.flush();
						stopClient();
					} catch (IOException e) {
						System.out.println("name in use, unable to send error");
						e.printStackTrace();
					}
				}
			}name = words[1];
		} else {
			System.out.println("Naam is niet meegegeven.");
		}

		System.out.println("Writing confirm to client");
		try {
			out.write(String.format(Protocol.CONFIRM + " "));
			out.newLine();
			out.flush();
		} catch (IOException e) {
			stopClient();
		}
	}

	/**
	 * Write start game to the socket with the playernames. The first
	 * playername is the starting player. Sets the status of the 
	 * clientConnection on playing. When it can't write to the socket. 
	 * The stopClient method will be called.
	 * @param name1 String of the playerName that will start
	 * @param name2 the name of the playerName that will be next
	 */
	/*
	 * @ requires name1 != null && name2 != null;
	 * @ ensures this.status.equals(Status.Playing);
	 */
	public void sentStartGame(String name1, String name2) {
		try {
			out.write(String.format(Protocol.START, name1 + " " + name2));
			out.newLine();
			out.flush();
			System.out.println("Write " + Protocol.START + " to client");
			this.status = Status.PLAYING;
		} catch (IOException e) {
			stopClient();
		}

	}

	/**
	 * It checks if the line is a correct move. If it is a correct move,
	 * it calls the doMove method from the clientController.
	 * @param line String that is received from the socket.
	 * @throws IOException
	 */
	/*
	 * @ requires line != null;
	 * @ requires line.split(" ").length == 4; 
	 * @ ensures int x == Integer.parseInt(line.split(" ")[2];
	 * @ ensures int y == Integer.parseInt(line.split(" ")[3];
	 */
	public void doMove(String line) throws IOException {
		String[] values = line.split(" ");
		if (values.length == 4) {
			try {
				int x = Integer.parseInt(values[2]);
				int y = Integer.parseInt(values[3]);
				ch.doMove(this, x, y);
			} catch (NumberFormatException e) {
				out.write(String.format(Protocol.ERROR, Protocol.Error.ILLEGAL_MOVE.code));
				out.newLine();
				out.flush();
			}
		} else {
			out.write(String.format(Protocol.ERROR, Protocol.Error.ILLEGAL_MOVE.code));
			out.newLine();
			out.flush();
		}
	}

	/**
	 * Write the move to the socket. If that does not work, it will stop the 
	 * clientConnection.
	 * @param player String name of the player from which the socket receives the move
	 * @param nextPlayer String name of the other player
	 * @param x integer the move at the x-axis
	 * @param y integer the move at the y-axis
	 */
	/*
	 * @ requires player != null && nextPlater != null; 
	 * @ requires x >= 0 && x <= 3 && y >= 0 && y <= 3;
	 */
	/*@pure*/public void sentMove(String player, String nextPlayer, int x, int y) {
		try {
			System.out.println("Sending move to client: " + x + " " + y);
			out.write(String.format(Protocol.SERVER_MOVE, player, x, y, nextPlayer));
			out.newLine();
			out.flush();
		} catch (IOException e) {
			stopClient();
		}
	}

	/**
	 * Write the winner to the socket. If that does not work, it will stop 
	 * the clientConnection. The status will be adapt to connect.
	 * @param winnerName String of the name of the winner
	 */
	//@ ensures this.getStatus().equals(Status.CONNECT);
	public void sentWinner(String winnerName) {
		System.out.println("Sending winner to client: " + winnerName);
		try {
			out.write(String.format(Protocol.END_WINNER, winnerName));
			out.newLine();
			out.flush();
		} catch (IOException e) {
			stopClient();
		}
		this.status = Status.CONNECT;
	}

	/**
	 * Write draw to the socket. If that does not work, it will stop 
	 * the clientConnection.
	 */
	//@ ensures this.getStatus().equals(Status.CONNECT);
	public void sentDraw() {
		try {
			out.write(String.format(Protocol.END_DRAW));
			out.newLine();
			out.flush();
		} catch (IOException e) {
			stopClient();
		}
		this.status = Status.CONNECT;
	}

	/**
	 * Write a disconnect error to the socket. If that does not work, it will stop 
	 * the clientConnection.
	 */
	//@ ensures this.getStatus().equals(Status.CONNECT);
	public void sentPlayerDisconnect() {
		try {
			System.out.println("sending to client: " + String.format(Protocol.ERROR, 110));
			out.write(String.format(Protocol.ERROR, 110));
			out.newLine();
			out.flush();
		} catch (IOException e) {
			stopClient();
		}
		this.status = Status.CONNECT;
	}

	/**
	 * Close the bufferedwriter and bufferedreader of the socket and
	 * call the removeClient method in ClientController.
	 */
	public void stopClient() {
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			System.out.println("Unable to properly close connections, lost connection with client, stopping client...");
		}
		running = false;
		ch.removeClient(this);
	}

	/**
	 * @return String of the name of the client.
	 */
	
	//@ ensures \result != null;
	/*@pure*/public String getClientName() {
		return name;
	}

	/**
	 * @return status of the client.
	 */
	//@ ensures \result != null;
	/*@pure*/public Status getStatus() {
		return status;
	}
}
