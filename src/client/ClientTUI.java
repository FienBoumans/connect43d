package client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import game.Piece;
import players.ComputerPlayer;

public class ClientTUI implements Observer {

	private Client client;

	public ClientTUI(Client client) {
		this.client = client;
	}

	public void start() {
		boolean running = true;
		Scanner scanner = new Scanner(System.in);
		printMessage("Type in: connect [nickname] [address] [port] [playertype]");
		printMessage("\nThe playertype is: H (humanplayer), E (easy computerplayer), D (difficult computerplayer)");
		printMessage("When you are ready for playing a game, type: ready");
		printMessage("If you want a hint, type: hint");
		while (running) {
			String line = scanner.nextLine();
			handleCommand(line);
		}
	}

	public void handleCommand(String s) {
		String[] split = s.split(" ");
		String command = split[0];
		State gameState = client.getGameState();

		synchronized (gameState) {
			if (command.equalsIgnoreCase("CONNECT")) {
				if (gameState != State.DISCONNECTED) {
					printMessage("already connected");
					printMessage("to disconnect enter: DISCONNECT");
				} else if (split.length >= 4) {
					try {
						String name = split[1];
						InetAddress address = InetAddress.getByName(split[2]);
						int port = Integer.parseInt(split[3]);
						String playerType = (split.length == 5) ? split[4] : "H";

						client.setPlayerType(playerType);
						client.setPlayer(playerType);
						client.setName(name);
						client.connect(name, address, port);
					} catch (NumberFormatException | UnknownHostException e) {
						printInvalidCommand(s);
					}
				} else {
					printInvalidCommand(s);
				}
			} else if (command.equalsIgnoreCase("DISCONNECT")) {
				if (gameState == State.DISCONNECTED) {
					printMessage("already disconnected");
				} else {
					client.disconnect();
				}
			} else if (command.equalsIgnoreCase("READY")) {
				if (gameState != State.CONNECTED) {
					printMessage("cannot ready now");
				} else {
					client.ready();
				}
			} else if (command.equalsIgnoreCase("UNREADY")) {
				if (gameState != State.READY) {
					printMessage("can only unready when ready");
				} else {
					client.unready();
				}
			} else if (command.equalsIgnoreCase("HINT")) {
				printMessage("hint: " + client.hint());
			} else if (command.equalsIgnoreCase("MOVE") && client.getPlayerType().equals("H")) {
				if (gameState != State.PLAY) {
					printMessage("can only do moves when in a game");
				} else if (split.length != 2) {
					printInvalidCommand(s);
				} else {
					try {
						int move = Integer.parseInt(split[1]);

						client.humanMove(move);
					} catch (NumberFormatException e) {
						printInvalidCommand(s);
					}
				}
			} else {
				printInvalidCommand(s);
			}
		}
	}

	public void printMessage(String message) {
		System.out.println(message);
	}

	private void printInvalidCommand(String s) {
		printMessage("invalid command: " + s);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		String command = (String) arg1;

		if (command.equals(Game.BOARD_UPDATE)) {
			Game game = (Game) arg0;
			printMessage(game.getBoard().toString());
		} else if (command.equals(Game.PLAYER_UPDATE)) {
			Game game = (Game) arg0;
			String currentPlayer = game.getCurrentPlayer();

			if (currentPlayer.equals(client.getName())) {
				if (client.getPlayerType().equals("H")) {
					printMessage("it is your turn");
				} else {
					printMessage("it is your turn");
					client.otherMove();
				}
			} else {
				printMessage("it is " + currentPlayer + "'s turn");
			}
		}
	}
}
