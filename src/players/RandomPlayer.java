package players;

import java.util.Random;

import game.Board;
import game.Piece;

public class RandomPlayer extends Player {
	private final Random random = new Random();

	public RandomPlayer(String name, Piece piece) {
		super(name, piece);
	}
	
	public int determineMove(Board board) {
		int move = random.nextInt(Board.DIM_X * Board.DIM_Y); 
		
		while (board.isStackFull(move % Board.DIM_X, move / Board.DIM_Y)) {
			move = random.nextInt(Board.DIM_X * Board.DIM_Y);
		}
		
		return move;
	}
}
