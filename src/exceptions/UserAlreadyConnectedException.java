package exceptions;

public class UserAlreadyConnectedException extends ProtocolException {
	public UserAlreadyConnectedException(){
		super("user is already connected");
	}

}
