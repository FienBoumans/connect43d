package players;

import game.Board;
import game.Piece;
import game.ThreeInARow;

public class ComputerPlayer extends Player {

	public ComputerPlayer(String name, Piece piece) {
		super(name,piece);
	}

	@Override
	public int determineMove(Board board) {
		System.out.println("Er wordt een set aangeroepen");
		for (int i = 0; i < Board.DIM_X * Board.DIM_Y; i++) {
			Board copy = board.deepCopy();
			int x = i % Board.DIM_X;
			int y = i / Board.DIM_Y;
			if (copy.isValidPosition(x, y)) {
				copy.addPiece(x, y, super.getPiece());
				if (copy.isWinner(super.getPiece())) {
					System.out.println("Set " + i + " wordt teruggegeven");
					return i;
				}
			}
		}
		for (int j = 0; j < Board.DIM_X * Board.DIM_Y; j++) {
			Board copy = board.deepCopy();
			int x = j % Board.DIM_X;
			int y = j / Board.DIM_Y;
			if (copy.isValidPosition(x, y)) {
				copy.addPiece(x, y, super.getPiece().other());
				if (copy.isWinner(super.getPiece().other())) {
					System.out.println("Set " + j + " wordt teruggegeven");
					return j;
				}
			}
		}
		

		for (int k = 0; k < Board.DIM_X * Board.DIM_Y; k++){
			Board copy = board.deepCopy();
			ThreeInARow b = new ThreeInARow(copy, super.getPiece().other());
			int x = k % Board.DIM_X;
			int y = k / Board.DIM_Y;
			if (copy.isValidPosition(x, y)) {
				copy.addPiece(x, y, super.getPiece().other());
				if(b.hasTwo3InARows()){
					System.out.println("Set " + k + " wordt teruggegeven");
					return k;
				}
			}
		}
		
		for (int k = 0; k < Board.DIM_X * Board.DIM_Y; k++){
			Board copy = board.deepCopy();
			ThreeInARow b = new ThreeInARow(copy, super.getPiece());
			int x = k % Board.DIM_X;
			int y = k / Board.DIM_Y;
			if (copy.isValidPosition(x, y)) {
				copy.addPiece(x, y, super.getPiece());
				if(b.hasTwo3InARows()){
					System.out.println("Set " + k + " wordt teruggegeven");
					return k;
				}
			}
		}
		
		for (int l = 0; l < Board.DIM_X * Board.DIM_Y; l++){
			Board copy = board.deepCopy();
			ThreeInARow b = new ThreeInARow(copy, super.getPiece());
			int x = l % Board.DIM_X;
			int y = l / Board.DIM_Y;
			if (copy.isValidPosition(x, y)) {
				copy.addPiece(x, y, super.getPiece());
				if(b.has3X(super.getPiece()) || b.has3Y(super.getPiece()) || b.has3Z(super.getPiece()) 
						|| b.has3XY(super.getPiece()) || b.has3YZ(super.getPiece()) 
						|| b.has3XZ(super.getPiece()) || b.has3XYZ(super.getPiece())){
					System.out.println("Set " + l + " wordt teruggegeven");
					return l;
				}
			}
		}
		
		int otherSet = new RandomPlayer(super.getName(), super.getPiece()).determineMove(board);
		System.out.println("Set " + otherSet + " wordt teruggegeven");
		return otherSet;
	}
}
