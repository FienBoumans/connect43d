package client;

import exceptions.*;
import protocol.Protocol;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class ServerConnection implements Runnable {
    private Client client;
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;

    public ServerConnection(Client client, Socket socket) throws IOException {
        this.client = client;
        this.socket = socket;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        System.out.println("running");

        try {
           String msg = in.readLine();
            while (msg != null) {
                System.out.println("message: ");
                handleCommand(msg);

                msg = in.readLine();
            }

            shutDown();
        } catch (IOException e) {
            shutDown();
        } catch (ProtocolException e) {
            e.printStackTrace();
            shutDown();
        }
    }

    public void handleCommand(String l) throws ProtocolException {
        String[] split = l.split(" ");
        System.out.println("receive from server: " + l);

        if (l.startsWith(Protocol.START_STRING)) {
            String[] playerNames = Arrays.copyOfRange(split, 2, split.length);

            System.out.println("start");

            client.startGame(playerNames);
        } else if (l.startsWith(Protocol.SERVER_MOVE_STRING)) {
            if (split.length == 6) {
                String player = split[2];
                int x = Integer.parseInt(split[3]);
                int y = Integer.parseInt(split[4]);
                String nextPlayer = split[5];

                client.serverMove(player, x, y, nextPlayer);
            } else if (split.length == 5) {
                String player = split[2];
                int x = Integer.parseInt(split[3]);
                int y = Integer.parseInt(split[4]);

                client.serverMove(player, x, y);
            }
        } else if (l.startsWith(Protocol.END_DRAW)) {
            client.endDraw();
        } else if (l.startsWith(Protocol.END_WINNER_STRING)) {
            String winner = "";

            if (split.length == 4) {
                winner = split[3];
            }

            client.endWinner(winner);
        } else if (l.startsWith(Protocol.ERROR_STRING)) {
        	System.out.println("Receive from server: " + l);
            int error = Integer.parseInt(split[1]);
            if (error==120) {
                throw new IllegalMoveException();
            } else if (error == 110) {
                throw new PlayerDisconnectException();
            } else if (error == 111){
                throw new IllegalMethodException();
            } else if (error == 190){
                throw new UserAlreadyConnectedException();
            } else {
                throw new ProtocolException("unknown error");
            }
        } else {
            throw new ProtocolException("invalid server command");
        }
    }

    public void receiveConfirm() throws IOException {
        String msg = in.readLine();
        if (msg == null || !msg.startsWith(Protocol.CONFIRM)) {
            throw new IOException();
        }
    }

    public void sendConnect(String name, String extensions) {
        sendMessage(String.format(Protocol.CONNECT, name, extensions));
    }

    public void sendDisconnect() {
        sendMessage(Protocol.DISCONNECT);
    }

    public void sendReady() {
        sendMessage(Protocol.READY);
    }

    public void sendUnready() {
        sendMessage(Protocol.UNREADY);
    }

    public void sendMove(int x, int y) {
        sendMessage(String.format(Protocol.CLIENT_MOVE, x, y));
    }

    public void sendMessage(String message) {
        try {
            out.write(message);
            out.newLine();
            out.flush();
        } catch (IOException e) {
            shutDown();
        }
    }

    public void shutDown() {
        try {
            socket.close();

            client.serverDisconnect();
        } catch (IOException e) {
            client.serverDisconnect();
        }
    }
}
