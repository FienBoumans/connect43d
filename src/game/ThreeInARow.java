package game;

public class ThreeInARow {
	Board board;
	Piece p;
	
	public ThreeInARow(Board board, Piece piece){
		this.board = board;
		p = piece;
	}
	
	public boolean has3X(Piece m) {
		int same;
		for (int z = 0; z < Board.DIM_Z; z++) {
			for (int y = 0; y < Board.DIM_Y; y++) {
				same = 0;
				for (int x = 0; x < Board.DIM_X; x++) {
					//System.out.println("X, XYZ : " + x + " " + y + " " + z);
					if (m.equals(board.getPiece(x, y, z))) {
						same++;
						if (same == 3) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean has3Y(Piece m) {
		int same;
		for (int z = 0; z < Board.DIM_Z; z++) {
			for (int x = 0; x < Board.DIM_X; x++) {
				same = 0;
				for (int y = 0; y < Board.DIM_Y; y++) {
					if (m.equals(board.getPiece(x, y, z))) {
						same++;
						if (same == 3) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean has3Z(Piece m) {
		int same;
		for (int x = 0; x < Board.DIM_X; x++) {
			for (int y = 0; y < Board.DIM_Y; y++) {
				same = 0;
				for (int z = 0; z < Board.DIM_Z; z++) {
					if (m.equals(board.getPiece(x, y, z))) {
						same++;
						if (same == 3) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean has3XY(Piece m) {
		int y;
		int same;
		for (int z = 0; z < Board.DIM_Z; z++) {
			same = 0;
			y = Board.DIM_Y - 1;
			for (int x = 0; x < Board.DIM_X; x++) {
				if (m.equals(board.getPiece(x, y, z))) {
					
					same++;
					if (same == 3) {
						return true;
					}
					y--;
				}
			}
		}
		for (int z = 0; z < Board.DIM_Z; z++) {
			same = 0;
			y = 0;
			for (int x = 0; x < Board.DIM_X; x++) {
				if (m.equals(board.getPiece(x, y, z))) {
					
					same++;
					if (same == 3) {
						return true;
					}
					y++;
				}
			}

		}
		return false;
	}

	public boolean has3YZ(Piece m) {
		int y;
		int same;
		for (int x = 0; x < Board.DIM_X; x++) {
			same = 0;
			y = Board.DIM_Y-1;
			for (int z = 0; z < Board.DIM_Z; z++) {
				//System.out.println("YZ, XYZ : " + x + " " + y + " " + z);
				if (m.equals(board.getPiece(x, y, z))) {
					
					same++;
					//System.out.println("same is "+same);
					if (same == 3) {
						return true;
					}
					y--;
				}
			}
		}
		for (int x = 0; x < Board.DIM_X; x++) {
			same = 0;
			y = 0;
			for (int z = 0; z < Board.DIM_Z; z++) {
				if (m.equals(board.getPiece(x, y, z))) {
					same++;
					if (same == 3) {
						return true;
					}
					y++;
				}
			}

		}
		return false;
	}

	public boolean has3XZ(Piece m) {
		int same;
		int x;
		for (int y = 0; y < Board.DIM_Y; y++) {
			same = 0;
			x = Board.DIM_X - 1;
			for (int z = 0; z < Board.DIM_Z; z++) {
				//System.out.println("XZ, XYZ : " + x + " " + y + " " + z);
				
				if (m.equals(board.getPiece(x, y, z))) {
					
					same++;
					if (same == 3) {
						return true;
					}
					x--;
				}
			}
		}
		for (int y = 0; y < Board.DIM_Y; y++) {
			same = 0;
			x = 0;
			for (int z = 0; z < Board.DIM_Z; z++) {
				//System.out.println("XZ, XYZ : " + x + " " + y + " " + z);
				if (m.equals(board.getPiece(x, y, z))) {
					
					same++;
					if (same == 3) {
						return true;
					}
					x++;
				}
			}

		}
		return false;
	}

	public boolean has3XYZ(Piece m) {
		int x = Board.DIM_X - 1;
		int z = 0;
		int same = 0;
		for (int y = 0; y < Board.DIM_Y; y++) {
			if (m.equals(board.getPiece(x, y, z))) {
				same++;
				if (same == 3) {
					return true;
				}
				x--;
				z++;
			}
		}

		int y = 0;
		z = 0;
		same = 0;
		for (x = 0; x < Board.DIM_X; x++) {
			//System.out.println("XYZ, XYZ : " + x + " " + y + " " + z);
			if (m.equals(board.getPiece(x, y, z))) {
				same++;
				if (same == 3) {
					return true;
				}
				y++;
				z++;
			}
		}

		x = Board.DIM_X - 1;
		y = Board.DIM_Y - 1;
		z = 0;
		same = 0;
		for (z = 0; z < Board.DIM_Z; z++) {
			//System.out.println("XYZ, XYZ : " + x + " " + y + " " + z);
			if (m.equals(board.getPiece(x, y, z))) {
				same++;
				if (same == 3) {
					return true;
				}
				y--;
				x--;
			}
		}

		x = 0;
		y = Board.DIM_Y - 1;
		same = 0;
		for (z = 0; z < Board.DIM_Z; z++) {
			//System.out.println("XYZ, XYZ : " + x + " " + y + " " + z);
			if (m.equals(board.getPiece(x, y, z))) {
				same++;
				if (same == 3) {
					return true;
				}
				y--;
				x++;
			}
		}

		return false;
	}
	
	public boolean hasTwo3InARows(){
		return has3X(p) && has3Y(p) || has3X(p) && has3Z(p) || has3X(p) && has3XY(p) || has3X(p) && has3XZ(p) || has3X(p) && has3YZ(p) || has3X(p) && has3XYZ(p)
				 || has3Y(p) && has3Z(p) || has3Y(p) && has3XY(p) || has3Y(p) && has3XZ(p) || has3Y(p) && has3YZ(p) || has3Y(p) && has3XYZ(p)
				 || has3Z(p) && has3XY(p) || has3Z(p) && has3XZ(p) || has3Z(p) && has3YZ(p) || has3Z(p) && has3XYZ(p)
				 || has3XY(p) && has3XZ(p) || has3XY(p) && has3YZ(p) || has3XY(p) && has3XYZ(p)
				 || has3XZ(p) && has3YZ(p) || has3XZ(p) && has3XYZ(p) || has3YZ(p) && has3XYZ(p);
		
	}
}
