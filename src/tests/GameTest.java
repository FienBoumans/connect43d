package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;

import client.Client;
import client.Game;
import game.Board;
import game.Piece;
import server.*;

public class GameTest {
	private Game game;
	private Client client = new Client();
	private String p1 = "player1";
	private String p2 = "player2";
	private Board b1;

	@Before
	public void setUp(){
		this.game = new Game(client, p1, p2);
	}

	@Test
	public void doMoveTest() {
		game.doMove(p1, 0, 0);
		assertEquals(game.getBoard().getPiece(0, 0, 0), Piece.YELLOW);
		game.doMove(p2, 1, 1);
		assertEquals(game.getBoard().getPiece(1, 1, 0), Piece.RED);
	}
	
	@Test
	public void getPieceTest(){
		assertEquals(game.getPiece(p1), Piece.YELLOW);
		assertEquals(game.getPiece(p2), Piece.RED);
	}
	
	@Test
	public void setBoardTest(){
		game.setBoard(b1);
		assertEquals(game.getBoard(), b1);
	}
	
	@Test
	public void setCurrentPlayerTest(){
		game.setCurrentPlayer(p1);
		assertEquals(game.getCurrentPlayer(), p1);
	}
}
