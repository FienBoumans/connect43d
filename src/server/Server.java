package server;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class Server {

	public static int askServerPort() {
		System.out.println("What port do you like to use?");
		Scanner s = new Scanner(System.in);
		int port = 0;
		do{
		if (s.hasNextLine()) {
			int nextInt = s.nextInt();
			if (nextInt > 1023 && nextInt < 65536) {
				port = nextInt;
			}
		}
		} while (port == 0);
		s.close();
		return port;
	}

	/** Start een Server-applicatie op. */
	public static void main(String[] args) {
		int port = askServerPort();

		System.out.println("Starting server...");
		Server server = new Server(port);
		server.listen();
	}

	private int port;
	private List<ClientController> threads;

	/** Constructs a new Server object */
	public Server(int port) {
		this.port = port;
		this.threads = new Vector<ClientController>();
	}

	/**
	 * Listens to a port of this Server if there are any Clients that would like
	 * to connect. For every new socket connection a new ClientHandler thread is
	 * started that takes care of the further communication with the Client.
	 */
	public void listen() {
		try {
			ClientController ch = new ClientController(port);
			ch.start();
		} catch (IOException e) {
			System.out.println("Listening for clients failed");
			e.printStackTrace();
		}
	}
}
