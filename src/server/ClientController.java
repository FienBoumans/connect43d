package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientController {
	private List<ClientConnection> connections = new ArrayList<>();
	private List<GameController> games = new ArrayList<>();
	private int port;

	/**
	 * Constructs a ClientHandler object Initialises both Data streams.
	 */
	// @ requires serverArg != null && sockArg != null;
	public ClientController(int port) throws IOException {
		this.port = port;
	}

	public void start() {
		System.out.println("Listening for incoming connections");
		ServerSocket serverSock;
		try {
			System.out.println("My address is: " + InetAddress.getLocalHost());
			serverSock = new ServerSocket(port);
			while (true) {
				Socket sock = serverSock.accept();
				System.out.println("Incoming connection received");
				ClientConnection cc = new ClientConnection(this, sock);
				connections.add(cc);
				cc.start();

			}
		} catch (IOException e) {
			System.out.println("Unable to create serversocket");
			e.printStackTrace();
		}

	}

	public void checkforOpponent(ClientConnection clientConnection) {
		System.out.println("all clients: " + connections);
		for (ClientConnection cc : this.getConnectionWithStatus(Status.READY)) {
			if (!cc.equals(clientConnection)) {
				System.out.println("Opponent found, starting game");
				GameController gh = new GameController(cc, clientConnection);
				games.add(gh);
				gh.startGame();
			}
		}
	}

	public void doMove(ClientConnection cc, int x, int y) {
		for (GameController g : games) {
			if (g.getPlayers().contains(cc)) {
				g.doSet(cc, x, y);
			}
		}
	}

	public List<ClientConnection> getConnectionWithStatus(Status status) {
		List<ClientConnection> clientConnections = new ArrayList<>();
		for (ClientConnection cc : connections) {
			if (cc.getStatus().equals(status)) {
				clientConnections.add(cc);
			}
		}
		return clientConnections;
	}
	
	public List<ClientConnection> getConnections(){
		return connections;
	}

	public void removeClient(ClientConnection cc) {
		if (cc.getStatus().equals(Status.PLAYING)) {
			for (GameController g : games) {
				g.playerDisconnected(cc);
				//TODO: game thread stoppen?
			}
		}
		connections.remove(cc);
	}
}
