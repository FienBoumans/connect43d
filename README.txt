Sentences which start with a -> are actions, the rest is extra explanation. 
To play the game:
-> Run the server, from the package server
The server ask to give a port
-> Choose an integer as port
-> Run a Client, from the package client (on the same of other computer) 
Instruction on what you have to type to the console is printed in the console
If you want to play as a humanplayer, so you have to make the moves, you write H
If you want to play as an Easy computerplayer, you write E
If you want to play as a difficult computerplayer, you write D
-> Type in: Connect yourname ip_address_of_server_computer portnumber H/D/E
-> Run another Client (on the same of different computer) and repeat the action mentioned above
To start a game
-> Both Clients have to type: ready 
In the console is printed who's turn it is
To make a move(if you are playing as a HumanPlayer)
-> Type: Move int_of_field_where_you_want_to_place_your_piece
-> keep playing till the game is finished.
