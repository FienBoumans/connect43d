package players;

import game.Board;
import game.Piece;

public class HumanPlayer extends Player {
	public HumanPlayer(String name, Piece piece) {
		super(name, piece);
	}

	// For making a move of a human player, this function will not be used.
	public int determineMove(Board board) {
		return 0;
	}
	
	private boolean checkMove(int choice, Board b) {
		int x = choice % Board.DIM_X;
		int y = choice / Board.DIM_Y;
		boolean valid = b.isValidMove(x, y);
		if (!valid) {
			 System.out.println("ERROR: field " + choice
			 + " is no valid choice.");
		}
		return valid;
	}
}