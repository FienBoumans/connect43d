package server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import game.Board;
import game.Piece;
import protocol.Protocol;

public class GameController {
	
	private ClientConnection cc1;
	private ClientConnection cc2;
	private Board board;
	private Piece cc1piece = Piece.RED;
	private Piece cc2piece = Piece.YELLOW;
	private Piece current = Piece.RED;

	/**
	 * Constructs a gameController object and set the 
	 * ClientConnections cc1 and cc2
	 * @param c1 ClientConnection of player 1
	 * @param c2 ClientConnection of player 2
	 */
	public GameController(ClientConnection c1, ClientConnection c2) {
		cc1 = c1;
		cc2 = c2;
	}

	/**
	 * Constructs a new board and calls the method sentStartGame for the 
	 * both clientConnections
	 */
	public void startGame() {
		board = new Board();
		cc1.sentStartGame(cc1.getClientName(), cc2.getClientName());
		cc2.sentStartGame(cc1.getClientName(), cc2.getClientName());

	}

	/**
	 * Determines the piece of the clientConnection and checks if the
	 * move is received from the player that is on turn and if it is 
	 * a valid move by calling the method isValidMove from the class 
	 * board.
	 * If the move is valid, the method sentSet is called and the 
	 * currentplayer will be switched. Also there will be checked if 
	 * there is a winner or the board is full by calling the method
	 * hasWinner and isBoardFull of the class board.
	 * @param cc ClientConnection should be a player of this game
	 * @param x integer move of the x-axis
	 * @param y integer move of the y-axis
	 */
	//@ requires cc != null;
	//@ requires x >= 0 && x <= 3 && y >= 0 && y <= 3;
	public void doSet(ClientConnection cc, int x, int y) {
		Piece playedPiece = cc.equals(cc1) ? cc1piece : cc2piece;
		if (current.equals(playedPiece) && board.isValidMove(x, y)) {
			board.addPiece(x, y, playedPiece);
			// send set to other player(s)
			sentSet(cc, x, y);
			current = current.other();

			if (board.hasWinner()) {
				System.out.println("Sent winner to clients");
				if (board.isWinner(cc1piece)) {
					cc1.sentWinner(cc1.getClientName());
					cc2.sentWinner(cc1.getClientName());
				} else {
					cc1.sentWinner(cc1.getClientName());
					cc2.sentWinner(cc1.getClientName());
				}
			} else if (board.isBoardFull()) {
				System.out.println("Sent draw to clients");
				cc1.sentDraw();
				cc2.sentDraw();
			}
		}
	}

	/**
	 * @return List of the 2 ClientConnections who are in the game
	 */
	//@ ensures \result != null;
	//@ ensures \result.size() == 2;
	public List<ClientConnection> getPlayers() {
		return Arrays.asList(cc1, cc2);
	
	}

	/**
	 * sent the move to both players of the game by calling
	 * the method sentMove of the class ClientConnection
	 * @param cc clientConnection that made the move
	 * @param x integer move of the x-axis
	 * @param y integer move of the y-axis
	 */
	/* 
	 * @ requires x >= 0 && x < 4;
	 * @ requires y >= 0 && y < 4;
	 * 
	 */
	public void sentSet(ClientConnection cc, int x, int y) {
		ClientConnection otherClient = cc.equals(cc1) ? cc2 : cc1;
		cc.sentMove(cc.getClientName(), otherClient.getClientName(), x, y);
		otherClient.sentMove(cc.getClientName(), otherClient.getClientName(), x, y);
	}
	
	/**
	 * Send the other player of the game that the opponent has 
	 * disconnect by calling the method sentPlayerDisconnect of 
	 * the class clientConnection
	 * @param cc ClientConnection of player that disconnected
	 */
	/*
	 * @ requires cc != null;
	 * @ requires cc.equals(cc1) || cc.equals(cc2);
	 * @ ensures otherClient.equals(!cc);
	 */
	public void playerDisconnected(ClientConnection cc) {
		ClientConnection otherClient = cc.equals(cc1) ? cc2 : cc1;
		otherClient.sentPlayerDisconnect();
	}
}
