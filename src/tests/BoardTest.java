package tests;

import game.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {
	private Board board;

	@Before
	public void setUp() {
		this.board = new Board();
	}
	
	@Test
	public void testDeepboardCopy(){
		board.addPiece(1, 1, Piece.RED);
		assertEquals(board.getPiece(0, 0, 0), board.deepCopy().getPiece(0, 0, 0));
		assertEquals(board.getPiece(1, 1, 0), board.deepCopy().getPiece(1, 1, 0));
	}

	@Test
	public void testGetPiece(){
		assertNull(board.getPiece(0, 1, 0));
		board.addPiece(0, 1, Piece.RED);
		assertEquals(board.getPiece(0, 1, 0), Piece.RED);
	}
	
	@Test
	public void testGetStackHeight() {
		assertEquals(board.getStackHeight(1, 1), 0);
		board.addPiece(1, 1, Piece.RED);
		assertEquals(board.getStackHeight(1, 1), 1);
	}

	@Test
	public void testIsStackFull() {
		assertFalse(board.isStackFull(2, 2));
		board.addPiece(2, 2, Piece.RED);
		board.addPiece(2, 2, Piece.RED);
		board.addPiece(2, 2, Piece.RED);
		assertFalse(board.isStackFull(2, 2));
		board.addPiece(2, 2, Piece.RED);
		assertTrue(board.isStackFull(2, 2));
	}

	@Test
	public void testIsValidStack() {
		assertTrue(board.isValidStack(0, 0));
		assertFalse(board.isValidStack(-1, 0));
		assertFalse(board.isValidStack(5, 0));
		assertTrue(board.isValidStack(2, 1));
		assertFalse(board.isValidStack(1, 4));
		assertFalse(board.isValidStack(1, -4));

	}
	
	@Test
	public void testIsValidMove(){
		assertTrue(board.isValidMove(0, 0));
		assertFalse(board.isValidMove(-1, 0));
		assertFalse(board.isValidMove(0, -1));
		assertFalse(board.isValidMove(5, 0));
		assertFalse(board.isValidMove(0, 5));
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		assertFalse(board.isValidMove(1, 0));
	}
	
	@Test
	public void testIsValidPosition(){
		assertTrue(board.isValidPosition(0, 0));
		assertFalse(board.isValidPosition(-1, 0));
		assertFalse(board.isValidPosition(0, -1));
		assertFalse(board.isValidPosition(5, 0));
		assertFalse(board.isValidPosition(0, 5));
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		board.addPiece(1, 0, Piece.YELLOW);
		assertFalse(board.isValidPosition(1, 0));
	}

	@Test
	public void testIsBoardFull() {
		assertFalse(board.isBoardFull());
		for (int k = 0; k < 4; k++) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					board.addPiece(i, j, Piece.RED);
				}
			}
		}
		assertTrue(board.isBoardFull());
	}
	
	@Test
	public void testAddPiece(){
		board.addPiece(1, 0, Piece.RED);
		assertEquals(board.getPiece(1, 0, 0), Piece.RED);
		assertFalse(board.addPiece(2, 0, Piece.RED));
	}
	
	@Test
	public void testHasRowX(){
		assertFalse(board.hasRowX(Piece.RED));
		board.addPiece(0, 0, Piece.RED);
		board.addPiece(1, 0, Piece.RED);
		board.addPiece(2, 0, Piece.RED);
		assertFalse(board.hasRowX(Piece.RED));
		board.addPiece(3, 0, Piece.RED);
		assertTrue(board.hasRowX(Piece.RED));
		assertFalse(board.hasRowX(Piece.YELLOW));
	}
	
	@Test
	public void testHasRowY(){
		assertFalse(board.hasRowY(Piece.RED));
		board.addPiece(0, 1, Piece.RED);
		board.addPiece(0, 2, Piece.RED);
		board.addPiece(0, 3, Piece.RED);
		assertFalse(board.hasRowY(Piece.RED));
		board.addPiece(0, 0, Piece.RED);
		assertTrue(board.hasRowY(Piece.RED));
		assertFalse(board.hasRowY(Piece.YELLOW));	
	}
	
	@Test
	public void testHasRowZ(){
		assertFalse(board.hasRowZ(Piece.RED));
		board.addPiece(0, 0, Piece.RED);
		board.addPiece(0, 0, Piece.RED);
		board.addPiece(0, 0, Piece.RED);
		assertFalse(board.hasRowZ(Piece.RED));
		board.addPiece(0, 0, Piece.RED);
		assertTrue(board.hasRowZ(Piece.RED));
		assertFalse(board.hasRowZ(Piece.YELLOW));
	}
	
	@Test
	public void testHasDiagonalXY(){
		assertFalse(board.hasDiagonalXY(Piece.RED));
		board.addPiece(0, 0, Piece.RED);
		board.addPiece(1, 1, Piece.RED);
		board.addPiece(2, 2, Piece.RED);
		assertFalse(board.hasDiagonalXY(Piece.RED));
		board.addPiece(3, 3, Piece.RED);
		assertTrue(board.hasDiagonalXY(Piece.RED));
		assertFalse(board.hasDiagonalXY(Piece.YELLOW));	
	}
	
	@Test
	public void testHasDiagonalXZ(){
		assertFalse(board.hasDiagonalXZ(Piece.RED));
		for (int i = 0; i<4; i++){
			board.addPiece(i, 0, Piece.RED);
		}
		
		for (int j = 1; j<4; j++){
			board.addPiece(j, 0, Piece.RED);
		}
		assertFalse(board.hasDiagonalXZ(Piece.RED));
		board.addPiece(2, 0, Piece.RED);
		board.addPiece(3, 0, Piece.RED);
		assertFalse(board.hasDiagonalXZ(Piece.RED));
		board.addPiece(3, 0, Piece.RED);
		assertTrue(board.hasDiagonalXZ(Piece.RED));
		assertFalse(board.hasDiagonalXZ(Piece.YELLOW));	
	}
	
	@Test
	public void testHasDiagonalYZ(){
		assertFalse(board.hasDiagonalYZ(Piece.RED));
		for (int i = 0; i<4; i++){
			board.addPiece(0, i, Piece.RED);
		}
		
		for (int j = 1; j<4; j++){
			board.addPiece(0, j, Piece.RED);
		}
		assertFalse(board.hasDiagonalYZ(Piece.RED));
		board.addPiece(0, 2, Piece.RED);
		board.addPiece(0, 3, Piece.RED);
		assertFalse(board.hasDiagonalYZ(Piece.RED));
		board.addPiece(0, 3, Piece.RED);
		assertTrue(board.hasDiagonalYZ(Piece.RED));
		assertFalse(board.hasDiagonalYZ(Piece.YELLOW));	
	}
	
	@Test
	public void testHasDiagonalXYZ(){
		assertFalse(board.hasDiagonalXYZ(Piece.RED));
		for (int i = 0; i<4; i++){
				board.addPiece(i, i, Piece.RED);
		}
		for (int j = 1; j<4; j++){
			board.addPiece(j, j, Piece.RED);
		}
		assertFalse(board.hasDiagonalXYZ(Piece.RED));
		board.addPiece(2, 2, Piece.RED);
		board.addPiece(3, 3, Piece.RED);
		assertFalse(board.hasDiagonalXYZ(Piece.RED));
		board.addPiece(3, 3, Piece.RED);
		assertTrue(board.hasDiagonalXYZ(Piece.RED));
		assertFalse(board.hasDiagonalXYZ(Piece.YELLOW));
	}
	
	@Test
	public void testIsWinnerDiagonal(){
		assertFalse(board.isWinner(Piece.YELLOW));
		for (int i = 0; i<4; i++){
			board.addPiece(i, i, Piece.RED);
		}
		assertTrue(board.isWinner(Piece.RED));
		assertFalse(board.isWinner(Piece.YELLOW));
	}
	
	@Test
	public void testIsWinnerRow(){
		assertFalse(board.isWinner(Piece.YELLOW));
		board.addPiece(1, 2, Piece.YELLOW);
		board.addPiece(1, 2, Piece.YELLOW);
		board.addPiece(1, 2, Piece.YELLOW);
		assertFalse(board.isWinner(Piece.YELLOW));
		board.addPiece(1, 2, Piece.YELLOW);
		assertTrue(board.isWinner(Piece.YELLOW));
		assertFalse(board.isWinner(Piece.RED));
	}
	
	@Test
	public void testHasWinner(){
		assertFalse(board.hasWinner());
		for (int i = 0; i<4; i++){
			board.addPiece(i, i, Piece.RED);
		}
		assertTrue(board.hasWinner());
	}
	
	@Test
	public void testGameOverWinner(){
		assertFalse(board.gameOver());
		for (int i = 0; i<4; i++){
			board.addPiece(i, i, Piece.RED);
		}
		assertTrue(board.gameOver());
	}
	
	@Test
	public void testGameOverBoard() {
		assertFalse(board.gameOver());
		for (int k = 0; k < 4; k++) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					board.addPiece(i, j, Piece.RED);
				}
			}
		}
		assertTrue(board.gameOver());
	}

}
