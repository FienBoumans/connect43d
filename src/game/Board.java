package game;

public class Board {
	public static final int DIM_X = 4;
	public static final int DIM_Y = 4;
	public static final int DIM_Z = 4;
	public static final int DIM = 4;

	private final Piece[][][] BOARD = new Piece[DIM_X][DIM_Y][DIM_Z];

	public Board() {
		reset();
	}

	/**
	 * Creates a deep copy of this board.
	 */
	/*
	 * @ ensures \result != this;
	 * @ ensures (\forall int x; 0 <= x & x < DIM_X;
	 * @ ensures (\forall int y; 0 <= y & y < DIM_Y;
	 * @ ensures (\forall int z; 0 <= z & z < DIM_Z;
	 * @ ensures \result.getPiece(x, y, z) == this.getField(x, y, z));
	 */

	public Board deepCopy() {
		Board board = new Board();
		for (int x = 0; x < DIM_X; x++) {
			for (int y = 0; y < DIM_Y; y++) {
				for (int z = 0; z < DIM_Z; z++) {
					board.BOARD[x][y][z] = BOARD[x][y][z];
				}
			}
		}

		return board;
	}

	/**
	 * Returns the content of the field (x, y, z).
	 * 
	 * @param x,
	 *            y, z
	 * @return the piece on the field
	 */
	// @ requires this.isValidPosition(x, y);
	// @ ensures \result == null || \result == Piece.YELLOW || \result ==
	// Piece.RED;
	/* @pure */

	public Piece getPiece(int x, int y, int z) {
		return BOARD[x][y][z];
	}

	/**
	 * Returns the stackheight of field (x, y).
	 * 
	 * @param x,
	 *            y
	 * @return the z value of the field (x, y).
	 */
	// @ requires this.isValidPosition(x, y);
	// @ ensures \result >= 0 && \result <= 3;
	/* @pure */

	public int getStackHeight(int x, int y) {
		int size = 0;

		for (int z = 0; z < DIM_Z; z++) {
			if (BOARD[x][y][z] == null) {
				break;
			}

			size++;
		}

		return size;
	}

	/**
	 * Returns true if the stack has reached it maximum height
	 * 
	 * @param x,
	 *            y
	 * @return true if getStackHeight(x,y) == DIM_Z.
	 */
	// @ requires this.isValidPosition(x, y);
	// @ ensures \result == (getStackHeight(x, y) == DIM_Z);
	/* @pure */

	public boolean isStackFull(int x, int y) {
		return getStackHeight(x, y) == DIM_Z;
	}

	/**
	 * Returns true if the it is a valid position in de x,y-place
	 * 
	 * @param x,
	 *            y
	 * @return true if x >=0 && x < DIM_X && y >=0 && y < DIM_Y
	 */
	// @ ensures \result == (0 =< x && x < DIM_X && y >= 0 && y < DIM_Y);
	/* @pure */
	public boolean isValidStack(int x, int y) {
		return x >= 0 && x < DIM_X && y >= 0 && y < DIM_Y;
	}

	/**
	 * Returns true if the move (x,y) pair refers to a valid field on the board.
	 * Does exactly the same as the isValidPosition(x, y) method.
	 *
	 * @return true if isValidStack(x,y) && !isStackFull(x,y)
	 */
	// @ ensures \result == (isValidStack(x,y) && !isStackFull(x,y));
	/* @pure */
	public boolean isValidMove(int x, int y) {
		return isValidPosition(x, y);
	}

	/**
	 * Returns true if board is completely full
	 * 
	 * @return true if forall (x,y) (isStackfull(x,y))
	 */
	// @ ensures \result == (\forall int x; x >= 0 && x < DIM_X; (\forall int y;
	// y >= 0 && y < DIM_Y; isStackFull(x,y);
	/* @pure */
	public boolean isBoardFull() {
		for (int x = 0; x < DIM_X; x++) {
			for (int y = 0; y < DIM_Y; y++) {
				if (!isStackFull(x, y)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Returns true if the game is over. The game is over when there is a winner
	 * or the whole board is full.
	 *
	 * @return true if the game is over
	 */
	// @ ensures \result == this.hasWinner() || this.isBoardFull();
	/* @pure */
	public boolean gameOver() {
		return hasWinner() || isBoardFull();
	}

	/**
	 * Returns true if the game has a winner. So either player Red or player
	 * Yellow has won.
	 *
	 * @return true if the game has a winner.
	 */
	// @ ensures \result == this.isWinner(Piece.RED) ||
	// this.isWinner(Piece.YELLOW);
	/* @pure */
	public boolean hasWinner() {
		return isWinner(Piece.RED) || isWinner(Piece.YELLOW);
	}

	/**
	 * Returns true if one of the players has a four connect.
	 *
	 * @return true if ...
	 */
	// @ ensures \result == this.hasRowX(piece) || this.hasRowY(piece) ||
	// this.hasRowZ(piece) || this.hasDiagonalXY(piece) ||
	// this.hasDiagonalYZ(piece) || this.hasDiagonalXZ(piece) ||
	// this.hasDiagonalXYZ(piece);
	/* @pure */
	public boolean isWinner(Piece piece) {
		return hasRowX(piece) || hasRowY(piece) || hasRowZ(piece) || hasDiagonalXY(piece) || hasDiagonalYZ(piece)
				|| hasDiagonalXZ(piece) || hasDiagonalXYZ(piece);
	}

	/**
	 * Checks whether there is a row long the x-axis which is full and only
	 * contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a row long the x-axis controlled by piece
	 */
	/* @ pure */
	public boolean hasRowX(Piece piece) {
		int same;
		for (int z = 0; z < DIM_Z; z++) {
			for (int y = 0; y < DIM_Y; y++) {
				same = 0;
				for (int x = 0; x < DIM_X; x++) {
					// System.out.println("X, XYZ : " + x + " " + y + " " + z);
					if (piece.equals(getPiece(x, y, z))) {
						same++;
						if (same == DIM_X) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether there is a row long the y-axis which is full and only
	 * contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a row long the y-axis controlled by piece
	 */
	/* @ pure */
	public boolean hasRowY(Piece piece) {
		int same;
		for (int z = 0; z < DIM_Z; z++) {
			for (int x = 0; x < DIM_X; x++) {
				same = 0;
				for (int y = 0; y < DIM_Y; y++) {
					// System.out.println("Y, XYZ : " + x + " " + y + " " + z);
					if (piece.equals(getPiece(x, y, z))) {
						same++;
						if (same == DIM_Y) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether there is a row long the z-axis which is full and only
	 * contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a row long the z-axis controlled by piece
	 */
	/* @ pure */
	public boolean hasRowZ(Piece piece) {
		int same;
		for (int x = 0; x < DIM_X; x++) {
			for (int y = 0; y < DIM_Y; y++) {
				same = 0;
				for (int z = 0; z < DIM_Z; z++) {
					// System.out.println("Z, XYZ : " + x + " " + y + " " + z);
					if (piece.equals(getPiece(x, y, z))) {
						same++;
						if (same == DIM_Z) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether there is a diagonal in the (x,y)-plane which is full and
	 * only contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a diagonal in the (x,y)-plane controlled by
	 *         piece
	 */
	/* @ pure */
	public boolean hasDiagonalXY(Piece piece) {
		int y;
		int same;
		for (int z = 0; z < DIM_Z; z++) {
			same = 0;
			y = DIM_Y - 1;
			for (int x = 0; x < DIM_X; x++) {
				// System.out.println("XY, XYZ : " + x + " " + y + " " + z);
				if (piece.equals(getPiece(x, y, z))) {

					same++;
					if (same == DIM) {
						return true;
					}
					y--;
				}
			}
		}
		for (int z = 0; z < DIM_Z; z++) {
			same = 0;
			y = 0;
			for (int x = 0; x < DIM_X; x++) {
				// System.out.println("XY, XYZ : " + x + " " + y + " " + z);
				if (piece.equals(getPiece(x, y, z))) {

					same++;
					if (same == DIM) {
						return true;
					}
					y++;
				}
			}

		}
		return false;
	}

	/**
	 * Checks whether there is a diagonal in the (y,z)-plane which is full and
	 * only contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a diagonal in the (y,z)-plane controlled by
	 *         piece
	 */
	/* @ pure */
	public boolean hasDiagonalYZ(Piece piece) {
		int y;
		int same;
		for (int x = 0; x < DIM_X; x++) {
			same = 0;
			y = DIM_Y - 1;
			for (int z = 0; z < DIM_Z; z++) {
				// System.out.println("YZ, XYZ : " + x + " " + y + " " + z);
				if (piece.equals(getPiece(x, y, z))) {
					same++;
					if (same == DIM) {
						return true;
					}
					y--;
				}
			}
		}
		for (int x = 0; x < DIM_X; x++) {
			same = 0;
			y = 0;
			for (int z = 0; z < DIM_Z; z++) {
				// System.out.println("YZ, XYZ : " + x + " " + y + " " + z);
				if (piece.equals(getPiece(x, y, z))) {
					same++;
					if (same == DIM) {
						return true;
					}
					y++;
				}
			}

		}
		return false;
	}

	/**
	 * Checks whether there is a diagonal in the (x,z)-plane which is full and
	 * only contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a diagonal in the (x,z)-plane controlled by
	 *         piece
	 */
	/* @ pure */
	public boolean hasDiagonalXZ(Piece piece) {
		int same;
		int x;
		for (int y = 0; y < DIM_Y; y++) {
			same = 0;
			x = DIM_X - 1;
			for (int z = 0; z < DIM_Z; z++) {
				// System.out.println("XZ, XYZ : " + x + " " + y + " " + z);

				if (piece.equals(getPiece(x, y, z))) {

					same++;
					if (same == DIM) {
						return true;
					}
					x--;
				}
			}
		}
		for (int y = 0; y < DIM_Y; y++) {
			same = 0;
			x = 0;
			for (int z = 0; z < DIM_Z; z++) {
				// System.out.println("XZ, XYZ : " + x + " " + y + " " + z);
				if (piece.equals(getPiece(x, y, z))) {

					same++;
					if (same == DIM) {
						return true;
					}
					x++;
				}
			}

		}
		return false;
	}

	/**
	 * Checks whether there is a 3 dimensional diagonal going through the
	 * (x,y,z)-field which is full and only contains the Piece piece.
	 *
	 * @param piece
	 * @return true if there is a diagonal in the (x,y,z)-field controlled by
	 *         piece
	 */
	/* @ pure */
	public boolean hasDiagonalXYZ(Piece piece) {
		int x = DIM_X - 1;
		int z = 0;
		int same = 0;
		for (int y = 0; y < DIM_Y; y++) {
			// System.out.println("XYZ, XYZ : " + x + " " + y + " " + z);
			if (piece.equals(getPiece(x, y, z))) {
				same++;
				if (same == DIM) {
					return true;
				}
				x--;
				z++;
			}
		}

		int y = 0;
		z = 0;
		same = 0;
		for (x = 0; x < DIM_X; x++) {
			if (piece.equals(getPiece(x, y, z))) {
				same++;
				if (same == DIM) {
					return true;
				}
				y++;
				z++;
			}
		}

		x = DIM_X - 1;
		y = DIM_Y - 1;
		z = 0;
		same = 0;
		for (z = 0; z < DIM_Z; z++) {
			if (piece.equals(getPiece(x, y, z))) {
				same++;
				if (same == DIM) {
					return true;
				}
				y--;
				x--;
			}
		}

		x = 0;
		y = DIM_Y - 1;
		same = 0;
		for (z = 0; z < DIM_Z; z++) {
			if (piece.equals(getPiece(x, y, z))) {
				same++;
				if (same == DIM) {
					return true;
				}
				y--;
				x++;
			}
		}

		return false;
	}

	/**
	 * Returns true if the (x,y) pair refers to a valid field on the board.
	 *
	 * @return true if isValidStack(x,y) && !isStackFull(x,y)
	 */
	// @ ensures \result == (isValidStack(x,y) && !isStackFull(x,y));
	/* @pure */
	public boolean isValidPosition(int x, int y) {
		return isValidStack(x, y) && !isStackFull(x, y);
	}

	public void reset() {
		for (int x = 0; x < DIM_X; x++) {
			for (int y = 0; y < DIM_Y; y++) {
				for (int z = 0; z < DIM_Z; z++) {
					BOARD[x][y][z] = null;
				}
			}
		}
	}

	/**
	 * Sets the content of field (x,y) to the Piece piece.
	 *
	 * @param x,y, piece
	 * @return true if the move makes four in a row
	 * @throws NotValidMoveException 
	 */
	// @ requires this.isValidPosition(x,y);
	// @ ensures this.getPiece(x,y,z) == piece;
	public boolean addPiece(int x, int y, Piece piece) {
		if (isValidPosition(x,y)){
			for (int z = 0; z < DIM_Z; z++) {
				if (BOARD[x][y][z] == null) {
					BOARD[x][y][z] = piece;
					return isWinner(piece);
				}
			} 
		}
		return false;
	}

	/**
	 * Returns a String of the board in the current situation. Each layer in the z-direction is displayed separately. 
	 *
	 * @return the game situation as String
	 */
	@Override
	public String toString() {
		String s = "\n";

		for (int z = 0; z < DIM_Z; z++) {
			String label = "<Layer " + z + ">";
			int width = 4 * DIM_X + 1;
			int padding = width - label.length();
			int padleft = padding / 2;
			s += String.format("%-" + width + "s", String.format("%" + (padleft + label.length()) + "s", label));
			s += "   ";
		}

		s += "\n\n";

		for (int y = 0; y < DIM_Y; y++) {
			for (int z = 0; z < DIM_Z; z++) {
				for (int x = 0; x < DIM_X; x++) {
					s += "+---";
				}
				s += "+   ";
			}

			s += "\n";

			for (int z = 0; z < DIM_Z; z++) {
				for (int x = 0; x < DIM_X; x++) {
					String p = (BOARD[x][y][z] == null) ? " " : ((BOARD[x][y][z] == Piece.RED) ? "R" : "Y");

					s += "| " + p + " ";
				}
				s += "|   ";
			}

			s += "\n";
		}

		for (int z = 0; z < DIM_Z; z++) {
			for (int x = 0; x < DIM_X; x++) {
				s += "+---";
			}
			s += "+   ";
		}

		return s;
	}
}