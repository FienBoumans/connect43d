package players;

import game.Board;
import game.Piece;

public abstract class Player {
	private String name;
	private Piece piece;
	
	public Player(String name, Piece piece) {
		this.name = name;
		this.piece = piece;
	}
	
	public String getName() {
		return name;
	}
	
	public Piece getPiece() {
		return piece;
	}
	
	public abstract int determineMove(Board board);
	
	public void makeMove(Board board) {
		int move = determineMove(board);
		int x = move % Board.DIM_X;
		int y = move / Board.DIM_Y;
		board.addPiece(x, y, piece);
	}
}

