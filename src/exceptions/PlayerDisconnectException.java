package exceptions;

public class PlayerDisconnectException extends ProtocolException {
	public PlayerDisconnectException() {
		super("player disconnected");
	}
}
